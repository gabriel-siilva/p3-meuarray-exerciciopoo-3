package br.cesed.si.p3.ed.listas.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import br.cesed.si.p3.ed.listas.MeuArrayList;
import br.cesed.si.p3.ed.listas.exceptions.IndiceInvalidoException;
import br.cesed.si.p3.ed.listas.exceptions.ListaVaziaException;
import br.cesed.si.p3.ed.listas.exceptions.ValorInvalidoException;

@DisplayName("Testes para a classe MeuArrayList")
class MeuArrayListTests {

	private MeuArrayList meuAl;

	@BeforeEach
	void setUp() {
		meuAl = new MeuArrayList();
	}

	@AfterEach
	void tearDown() {
		meuAl = null;
	}

	@DisplayName("size() com lista vazia")
	@Test
	void testSizeListaVazia() {
		assertEquals(0, meuAl.size());
	}

	@DisplayName("add() com objeto null")
	@Test
	void testAddObjetoNull() {
		Assertions.assertThrows(ValorInvalidoException.class, () -> {
			meuAl.add(null);
			meuAl.add(0, null);
		});
	}

	@DisplayName("add() test")
	@Test
	void testAddBasico() throws ValorInvalidoException {
		meuAl.add(1);
		meuAl.add(2);
		meuAl.add(3);

		Assert.assertEquals(3, meuAl.size());
	}

	@DisplayName("add() criando array maior")
	@Test
	void testAddArrayMaior() throws ValorInvalidoException {
		meuAl.add(1);
		meuAl.add(2);
		meuAl.add(3);
		meuAl.add(4);

		Assert.assertEquals(4, meuAl.size());
	}

	@DisplayName("add() com �ndice maior que o array")
	@Test
	void testAddIndiceException() throws IndiceInvalidoException {
		Assertions.assertThrows(IndiceInvalidoException.class, () -> {
			meuAl.add(4, "elemento");
		});
	}
	
	@DisplayName("add() com �ndice negativo")
	@Test
	void testAddIndiceNegativoException() throws IndiceInvalidoException {
		Assertions.assertThrows(IndiceInvalidoException.class, () -> {
			meuAl.add(-2, "elemento");
		});
	}
	
	@DisplayName("add() por indice")
	@Test
	void testAddIndice() throws IndiceInvalidoException, ValorInvalidoException {
		meuAl.add(2, "teste");
		
		assertEquals("teste", meuAl.getElement(2));
	}
	
	@DisplayName("set() com �ndice negativo")
	@Test
	void testSetIndiceNegativoException() throws IndiceInvalidoException {
		Assertions.assertThrows(IndiceInvalidoException.class, () -> {
			meuAl.add(-1, "elemento");
		});
	}
	
	@DisplayName("set() com �ndice maior que o array")
	@Test
	void testSetIndiceException() throws IndiceInvalidoException {
		Assertions.assertThrows(IndiceInvalidoException.class, () -> {
			meuAl.add(4, "elemento");
		});
	}
	
	@DisplayName("remove() por valor com lista vazia")
	@Test
	void testRemoveListaVazia() {
		Assertions.assertThrows(ListaVaziaException.class, () -> {
			meuAl.remove("objeto");
		});
	}

	@DisplayName("remove() test")
	@Test
	void testRemove() throws ValorInvalidoException, ListaVaziaException {
		meuAl.add("A");
		meuAl.add("B");
		meuAl.remove("B");
		
		assertEquals(1, meuAl.size());
	}
	
	@DisplayName("remove() por �ndice")
	@Test
	void testRemoveIndice() throws IndiceInvalidoException, ValorInvalidoException {
		meuAl.add("A");
		meuAl.add("B");
		meuAl.remove(1);
		
		assertEquals(null, meuAl.getElement(1));
		assertEquals(1, meuAl.size());
	}
	
	@DisplayName("remove() com indice maior que o array")
	@Test
	void testRemoveIndiceMaior() {
		Assertions.assertThrows(IndiceInvalidoException.class, () -> {
			meuAl.remove(5);
		});
	}
	
	@DisplayName("remove() com �ndice negativo")
	@Test
	void testRemoveIndiceNegativo() {
		Assertions.assertThrows(IndiceInvalidoException.class, () -> {
			meuAl.remove(-1);
		});
	}
	
	@DisplayName("getElement() com �ndice negativo")
	@Test
	void testGetElementIndiceNegativo() {
		Assertions.assertThrows(IndiceInvalidoException.class, () -> {
			meuAl.getElement(-1);
		});
	}
	
	@DisplayName("getElement() com �ndice maior que array")
	@Test
	void testGetElementIndiceMaior() {
		Assertions.assertThrows(IndiceInvalidoException.class, () -> {
			meuAl.getElement(5);
		});
	}
	
	@DisplayName("isEmpty() test")
	@Test
	void testIsEmpty() {
		assertEquals(true, meuAl.isEmpty());
	}
	
	@DisplayName("size() test")
	@Test
	void testSize() throws ValorInvalidoException {
		meuAl.add(1);
		meuAl.add(2);
		meuAl.add(3);
		
		assertEquals(3, meuAl.size());
	}
}
