package br.cesed.si.p3.ed.listas;

import br.cesed.si.p3.ed.listas.exceptions.IndiceInvalidoException;
import br.cesed.si.p3.ed.listas.exceptions.ListaVaziaException;
import br.cesed.si.p3.ed.listas.exceptions.ValorInvalidoException;

public class MeuArrayList {
	private static final int TAMANHO_INICIAL = 3;
	private Object[] meuArray = new Object[TAMANHO_INICIAL];
	private int inseridos;

	/**
	 * @param objeto
	 * @throws ValorInvalidoException
	 *             Adiciona um elemento na lista
	 */
	public void add(Object objeto) throws ValorInvalidoException {
		if (objeto == null) {
			throw new ValorInvalidoException();
		}

		if (inseridos == meuArray.length) {
			Object[] novoArray = new Object[meuArray.length + TAMANHO_INICIAL];

			for (int i = 0; i < meuArray.length; i++) {
				novoArray[i] = meuArray[i];
			}

			meuArray = novoArray;
		}

		meuArray[inseridos] = objeto;
		inseridos++;
	}

	/**
	 * @param indice
	 * @param objeto
	 * @throws IndiceInvalidoException
	 * @throws ValorInvalidoException
	 *             Adiciona um elemento na lista passando o �ndice
	 */
	public void add(int indice, Object objeto) throws IndiceInvalidoException, ValorInvalidoException {
		if (indice > meuArray.length || indice < 0) {
			throw new IndiceInvalidoException();
		}

		if (objeto == null) {
			throw new ValorInvalidoException();
		}

		meuArray[indice] = objeto;
		inseridos++;
	}

	/**
	 * @param indice
	 * @param objeto
	 * @throws IndiceInvalidoException
	 * @throws ValorInvalidoException
	 *             Altera o valor de um objeto na lista
	 */
	public void set(int indice, Object objeto) throws IndiceInvalidoException, ValorInvalidoException {
		if (indice > meuArray.length || indice < 0) {
			throw new IndiceInvalidoException();
		}

		if (objeto == null) {
			throw new ValorInvalidoException();
		}

		meuArray[indice] = objeto;
	}

	/**
	 * @param objeto
	 *            Remove um elemento da lista
	 * @throws ListaVaziaException 
	 */
	public void remove(Object objeto) throws ListaVaziaException {
		if (isEmpty()) {
			throw new ListaVaziaException();
		}
		
		int posEncontrada = -1;

		for (int i = 0; i < meuArray.length; i++) {
			if (meuArray[i].equals(objeto)) {
				posEncontrada = i;
				break;
			}
		}

		for (int i = posEncontrada; i < inseridos - 1; i++) {
			meuArray[i] = meuArray[i + 1];
		}

		meuArray[inseridos - 1] = null;
		inseridos--;
	}

	/**
	 * @param indice
	 *            Remove um elemento pelo �ndice da lista
	 * @throws IndiceInvalidoException 
	 */
	public void remove(int indice) throws IndiceInvalidoException {
		if (indice > meuArray.length || indice < 0) {
			throw new IndiceInvalidoException();
		}
		
		for (int i = indice; i < inseridos - 1; i++) {
			meuArray[i] = meuArray[i + 1];
		}

		meuArray[inseridos - 1] = null;
		inseridos--;
	}

	/**
	 * @param indice
	 *            Retorna um elemento da lista
	 * @throws IndiceInvalidoException
	 */
	public Object getElement(int indice) throws IndiceInvalidoException {
		if (indice > meuArray.length || indice < 0) {
			throw new IndiceInvalidoException();
		}
		return meuArray[indice];
	}

	/**
	 * Retorna se a lista est� vazia ou n�o
	 * 
	 * @return boolean
	 */
	public boolean isEmpty() {
		boolean empty = true;

		if (inseridos > 0) {
			empty = false;
		}

		return empty;
	}

	/**
	 * Retorna a quantidade de objetos na lista
	 */
	public int size() {
		return inseridos;
	}
}
